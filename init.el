;; User pack init file
;; !NOTE: using emacs live at top level...
;; Use this file to initiate the pack configuration.
;; See README for more information.
;;
;;
;; desiring rapid load of new docs into same window.
(require 'server)
(unless (server-running-p)
   (server-start))
;; el-get
(live-load-config-file "el-get-conf.el")

;; Load bindings config
(live-load-config-file "bindings.el")

;; smooth-scrolling
(live-load-config-file "smooth-scrolling.el")

;;evil
(live-load-config-file "evil-conf.el")

;;emacs-jabber
(live-load-config-file "emacs-jabber-conf.el")

;;sr-speedbar
(live-load-config-file "sr-speedbar-conf.el")

;;solarized
;;(live-load-config-file "color-theme-conf.el")

;;paredit
;;(live-load-config-file "paredit-conf.el")

;;screens nav
;; use Shift+arrow_keys to move cursor around split panes
(windmove-default-keybindings)

;; when cursor is on edge, move to the other side, as in a toroidal space
(setq windmove-wrap-around t )

;;cedet
;;(live-load-config-file "min-cedet-conf.el")

;; doxyemacs
(live-load-config-file "doxymacs-conf.el")

;; rainbow-mode
(live-load-config-file "rainbow-mode-conf.el")
;;
;;
;; !TODO: org agenda stuff, line-wrapping...
(live-load-config-file "org-conf.el")
;; rainbow-delimiters
(live-load-config-file "rainbow-delimiters-conf.el")

(global-visual-line-mode 1)

;; indent
(setq-default indent-tabs-mode nil)
  (setq default-tab-width 4) ; or any other preferred value
      (defvaralias 'c-basic-offset 'tab-width)
      (defvaralias 'standard-indent 'tab-width)
      (defvaralias 'cperl-indent-level 'tab-width)
      (setq c-default-style "linux")
;; line highlighting
(global-hl-line-mode 1)
;; line number
(global-linum-mode 1)
  ;;
