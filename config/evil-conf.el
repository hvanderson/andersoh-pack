;;evil mode (vim)
(live-add-pack-lib "evil")
(require 'evil)
(evil-mode 1)
;;; esc quits
;;;
(define-key evil-normal-state-map [escape] 'keyboard-quit)
(define-key evil-visual-state-map [escape] 'keyboard-quit)
(define-key minibuffer-local-map [escape] 'minibuffer-keyboard-quit)
(define-key minibuffer-local-ns-map [escape] 'minibuffer-keyboard-quit)
(define-key minibuffer-local-completion-map [escape]
  'minibuffer-keyboard-quit)
(define-key minibuffer-local-must-match-map [escape]
  'minibuffer-keyboard-quit)
(define-key minibuffer-local-isearch-map [escape] 'minibuffer-keyboard-quit)

;;evil mode conflicts at points with ecb
(define-key evil-motion-state-map (kbd "RET") nil)
(define-key evil-normal-state-map (kbd "RET") 'evil-ret)

(add-hook 'ecb-history-buffer-after-create-hook 'evil-motion-state)
(add-hook 'ecb-methods-buffer-after-create-hook 'evil-motion-state)
(add-hook 'ecb-sources-buffer-after-create-hook 'evil-motion-state)
