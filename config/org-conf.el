(require 'org)
;;make org-mode work with files ending in .org
(add-to-list 'auto-mode-alist '("\\.org$" . org-mode))

(custom-set-variables
  ;; custom-set-variables was added by Custom.
  ;; If you edit it by hand, you could mess it up, so be careful.
  ;; Your init file should contain only one such instance.
  ;; If there is more than one, they won't work right.
 ;;'(inhibit-startup-screen t)
 '(initial-scratch-message nil)
 '(org-agenda-files (quote ("~/logs/org/hca_agenda.org"))))
(custom-set-faces
  ;; custom-set-faces was added by Custom.
  ;; If you edit it by hand, you could mess it up, so be careful.
  ;; Your init file should contain only one such instance.
  ;; If there is more than one, they won't work right.
 )
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; Capture
;; Capture
(global-set-key (kbd "C-c r") 'org-capture)
(setq org-capture-templates
      '(("t" "Agenda Todo" entry
	 (file+headline "~/logs/org/hca_agenda.org" "Agenda")
	 "\n\n** TODO %?\n%T\n\n%i\n%a\n\n\n"
	 :empty-lines 1)

	("n" "Agenda Notes" entry
	 (file+headline "~/logs/org/hca_agenda.org" "Agenda")
	 "\n\n** %?\n%T\n%i\n%a\n\n\n"
	 :empty-lines 1)))
(org-agenda-list)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; Popups
;; Get appointments for today
(defun my-org-agenda-to-appt ()
  (interactive)
  (setq appt-time-msg-list nil)
  (run-at-time "24:01" nil 'my-org-agenda-to-appt)
  (let ((org-deadline-warning-days 0))    ;; will be automatic in org  5.23
    (org-agenda-to-appt)))

;;; Disabled so that I can open multiple emacs without org loading agenda files.
(appt-activate t)

;; 5 minute warning
;(setq appt-message-warning-time '60)
;(setq appt-display-interval '15)

;; Update appt each time agenda opened.
(add-hook 'org-finalize-agenda-hook 'my-org-agenda-to-appt)

;; Setup agenda popup, we tell appt to use window, and replace default function
;(setq appt-display-format 'window)
;(setq appt-disp-window-function (function my-appt-disp-window))

;(defun my-appt-disp-window (min-to-app new-time msg)
;  (save-window-excursion
;    (shell-command
;     (concat
;      "c:/windows/system32/cscript.exe //nologo c:/Org/Popup.vbs \"" msg "\"") nil nil)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; Timestamps
;; Insert immediate timestamp
(setq org-agenda-skip-additional-timestamps nil)
(define-key global-map (kbd "<f9>")
  '(lambda () (interactive)
              (when (eq major-mode 'org-mode)
                    (org-insert-time-stamp nil t t)
                    )))
;                    (insert "\n"))))
;; Insert inactive timestamp
(defun bh/insert-inactive-timestamp ()
  "Insert a timestamp for the current time at point."
  (interactive)
  (save-excursion
    (insert "\n")
    (org-cycle)
    (org-insert-time-stamp nil t t nil nil nil)))

