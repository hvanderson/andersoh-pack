;;emacs-jabber
(live-add-pack-lib "emacs-jabber")
(require 'jabber-autoloads)
(setq jabber-vcard-avatars-retrieve nil)
(setq jabber-vcard-avatars-publish nil)
(setq jabber-chat-buffer-show-avatar nil)
(setq my-chat-prompt "[%t] %n>\n")
(setq
 jabber-chat-foreign-prompt-format my-chat-prompt
    jabber-chat-local-prompt-format my-chat-prompt
       jabber-groupchat-prompt-format my-chat-prompt
          jabber-muc-private-foreign-prompt-format "[%t] %g/%n>\n"
             )
