;; el-get
;; from alexott, originally
(defcustom el-get-dir "/Users/andersoh/.live-packs/andersoh-pack/lib/"
  "Path where to install the packages."
    :group 'el-get
      :type 'directory)


(live-add-pack-lib "el-get/el-get/")
(require 'el-get)
;;(require 'el-get-status)
;;(setq el-get-byte-compile nil)
;;(setq el-get-recipe-path  '(concat (live-pack-lib-dir "el-get/recipes/")))
;;(setq el-get-sources '(
;;  emacs-jabber 
;;  json
;;  cmake-mode
;;  ))
;;(el-get 'sync)
;;
;;(el-get-update-all)
