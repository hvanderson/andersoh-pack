;; Place your bindings here.

;;sr-speedbar
(global-unset-key (kbd "<f8>"))
(global-set-key (kbd "<f8>") 'sr-speedbar-toggle)
