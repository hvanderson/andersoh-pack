(live-add-pack-lib "color-theme")

(require 'color-theme)

;;NOTE this would normally be handled by emacs-live distro
;; use solarize dark
(load-file(concat(live-pack-lib-dir) "solarized-theme/color-theme-solarized.el"))
(load-theme 'solarized-dark t)
