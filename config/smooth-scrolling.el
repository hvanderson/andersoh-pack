;;smooth scrolling
;;(live-add-pack-lib "smooth-scrolling")
;;(require 'smooth-scrolling)
(setq 
redisplay-dont-pause t
scroll-margin 1
scroll-step 1
auto-save-interval 1500 
scroll-conservatively 100000
scroll-up-aggressively 0.01
scroll-down-aggressively 0.01
scroll-preserve-screen-position 1)

;;(setq
;;scroll-margin 0                  
;;auto-save-interval 1500 
;;scroll-conservatively 100000
;;scroll-up-aggressively 0.01
;;scroll-down-aggressively 0.01
;;scroll-preserve-screen-position 1)

;; Scroll just one line when hitting bottom of window
;; ;; http://www.emacswiki.org/emacs/SmoothScrolling
;;(setq scroll-conservatively 10000)
;; ;;
;; ;; Scroll one line at a time (less "jumpy" than defaults)
;; ;; http://www.emacswiki.org/emacs/SmoothScrolling
;; (setq mouse-wheel-scroll-amount '(1 ((shift) . 1))) ; one line at a time
;; ;;(setq mouse-wheel-progressive-speed nil)      ; don't accelerate scrolling
;; (setq mouse-wheel-follow-mouse 't)          ; scroll window under mouse
;;(setq scroll-step 1)                  ; keyboard scroll one line at a time
;;
;;(setq scroll-conservatively 2000)

;;(setq scroll-step 1)

;;(setq scroll-margin 1)

;;(setq auto-window-vscroll nil)
    
;;(setq mouse-wheel-scroll-amount '(1 ((shift) . 1))) ;; one line at a time

;;(setq mouse-wheel-progressive-speed nil) ;; don't accelerate scrolling

;;(setq mouse-wheel-follow-mouse 't) ;; scroll window under mouse
; Autosave every 500 typed characters
;;(setq auto-save-interval 1500) ;; Scroll just one line when hitting bottom of window
;;(setq scroll-margin 1
;; scroll-conservatively 0
;; scroll-up-aggressively 0.01
;; scroll-down-aggressively 0.01)
