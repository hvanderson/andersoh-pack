;;cedet
(live-add-pack-lib "cedet")
(require 'cedet)
(require 'semantic/ia)
(require 'srecode)
(require 'semantic/bovine/gcc)
;;semantic
(add-to-list 'semantic-default-submodes 'global-semanticdb-minor-mode)
(add-to-list 'semantic-default-submodes 'global-semantic-mru-bookmark-mode)
(add-to-list 'semantic-default-submodes 'global-semantic-idle-scheduler-mode)
(add-to-list 'semantic-default-submodes 'global-semantic-idle-completions-mode)
(add-to-list 'semantic-default-submodes 'global-semantic-highlight-func-mode)
(add-to-list 'semantic-default-submodes 'global-semantic-decoration-mode)
(add-to-list 'semantic-default-submodes 'global-semantic-show-unmatched-syntax-mode)

(semantic-add-system-include "/opt/oblong/g-speak3.10/include" 'c++-mode)
(semantic-add-system-include "/opt/oblong/g-speak3.10/include" 'c-mode)
(semantic-add-system-include "/opt/oblong/greenhouse/include" 'c++-mode)
(semantic-add-system-include "/opt/oblong/greenhouse/include" 'c-mode)
(semantic-add-system-include "/opt/oblong/assimp/include" 'c++-mode)
(semantic-add-system-include "/opt/oblong/assimp/include" 'c-mode)
(semantic-add-system-include "/opt/oblong/deps/include" 'c++-mode)
(semantic-add-system-include "/opt/oblong/deps/include" 'c-mode)

;;(add-to-list 'semantic-default-submodes 'global-semantic-stickyfunc-mode)
;;THIS ONE NEEDED but fails?
;;(add-to-list 'semantic-default-submodes 'global-cedet-m3-minor-mode)
;;(add-to-list 'semantic-default-submodes 'global-semantic-highlight-edits-mode)
;;(add-to-list 'semantic-default-submodes 'global-semantic-show-parser-state-mode)

;; Activate semantic
(semantic-mode 1)

;;apparently, emacs24+ autoloads these??
;;(require 'semantic/bovine/c)
;;(require 'semantic/bovine/clang)

;; customisation of modes
(defun hans/cedet-hook ()
  (local-set-key [(control return)] 'semantic-ia-complete-symbol-menu)
  (local-set-key "\C-c?" 'semantic-ia-complete-symbol)
  (local-set-key "\C-c>" 'semantic-ia-complete-symbol-analyze-inline)
  (local-set-key "\C-c=" 'semantic-decoration-include-visit)
  (local-set-key "\C-cj" 'semantic-ia-fast-jump)
  (local-set-key "\C-cq" 'semantic-ia-show-doc)
  (local-set-key "\C-cs" 'semantic-ia-show-summary)
  (local-set-key "\C-cp" 'semantic-analyze-proto-impl-toggle)
  (local-set-key (kbd "C-c <left>") 'semantic-tag-folding-fold-block)
  (local-set-key (kbd "C-c <right>") 'semantic-tag-folding-show-block)
  (add-to-list 'ac-sources 'ac-source-semantic)
  (semantic-mode t)
  )
(add-hook 'semantic-init-hooks 'hans/cedet-hook)
(add-hook 'c-mode-common-hook 'hans/cedet-hook)
(add-hook 'lisp-mode-hook 'hans/cedet-hook)
(add-hook 'scheme-mode-hook 'hans/cedet-hook)
(add-hook 'emacs-lisp-mode-hook 'hans/cedet-hook)

(defun hans/c-mode-cedet-hook ()
  (local-set-key "." 'semantic-complete-self-insert)
  (local-set-key ">" 'semantic-complete-self-insert)
  (local-set-key "\C-ct" 'eassist-switch-h-cpp)
  (local-set-key "\C-xt" 'eassist-switch-h-cpp)
  (local-set-key "\C-ce" 'eassist-list-methods)
  (local-set-key "\C-c\C-r" 'semantic-symref)
  ;;(add-to-list 'ac-sources 'ac-source-gtags)
  (setq ac-sources (append '(ac-source-semantic) ac-sources))
  (local-set-key (kbd "RET") 'newline-and-indent)
  (linum-mode t)
  (semantic-mode t)
  )
(add-hook 'c-mode-common-hook 'hans/c-mode-cedet-hook)

;; Autocomplete
(require 'auto-complete-config)
(add-to-list 'ac-dictionary-directories
  (expand-file-name "~/.emacs.d/elpa/auto-complete-1.4.20110207/dict"))
(setq ac-comphist-file
  (expand-file-name "~/.emacs.d/ac-comphist.dat"))
(ac-config-default)

;;TODO: get this working... probably install global
;;(when (cedet-gnu-global-version-check t)
  ;;(semanticdb-enable-gnu-global-databases 'c-mode t)
  ;;(semanticdb-enable-gnu-global-databases 'c++-mode t))

;;TODO: get this working... probably install ectags
;;(when (cedet-ectag-version-check t)
 ;; (semantic-load-enable-primary-ectags-support))

;; SRecode
(global-srecode-minor-mode 1)

;; EDE
(global-ede-mode 1)
(ede-enable-generic-projects)

;;; MAKEFILE-type generic

(defclass ede-generic-makefile-project (ede-generic-project)
  ((buildfile :initform "Makefile")
   )
  "Generic Project for makefiles.")

(defmethod ede-generic-setup-configuration ((proj ede-generic-makefile-project) config)
  "Setup a configuration for Make."
  (oset config build-command "make -k")
  (oset config debug-command "gdb ")
  )

(ede-generic-new-autoloader "generic-makefile" "Make"
                       "Makefile" 'ede-generic-makefile-project)

;; helper for boost setup...
(defun c++-setup-boost (boost-root)
  (when (file-accessible-directory-p boost-root)
    (let ((cfiles (cedet-files-list-recursively boost-root "\\(config\\|user\\)\\.hpp")))
      (dolist (file cfiles)
        (add-to-list 'semantic-lex-c-preprocessor-symbol-file file)))))


;; my functions for EDE
(defun hans/ede-get-local-var (fname var)
  "fetch given variable var from :local-variables of project of file fname"
  (let* ((current-dir (file-name-directory fname))
         (prj (ede-current-project current-dir)))
    (when prj
      (let* ((ov (oref prj local-variables))
            (lst (assoc var ov)))
        (when lst
          (cdr lst))))))

;; setup compile package
(require 'compile)
(setq compilation-disable-input nil)
(setq compilation-scroll-output t)
(setq mode-compile-always-save-buffer-p t)

(defun hans/compile ()
  "Saves all unsaved buffers, and runs 'compile'."
  (interactive)
  (save-some-buffers t)
  (let* ((fname (or (buffer-file-name (current-buffer)) default-directory))
	 (current-dir (file-name-directory fname))
         (prj (ede-current-project current-dir)))
    (if prj
	(project-compile-project prj)
	(compile compile-command))))
(global-set-key [f9] 'hans/compile)

;;
(defun hans/gen-std-compile-string ()
  "Generates compile string for compiling CMake project"
  (let* ((current-dir (file-name-directory
                       (or (buffer-file-name (current-buffer)) default-directory)))
         (prj (ede-current-project current-dir))
         (root-dir (ede-project-root-directory prj)))
    (concat "cd " root-dir "; make -j2")))

;;
(defun hans/gen-cmake-debug-compile-string ()
  "Generates compile string for compiling CMake project in debug mode"
  (let* ((current-dir (file-name-directory
                       (or (buffer-file-name (current-buffer)) default-directory)))
         (prj (ede-current-project current-dir))
         (root-dir (ede-project-root-directory prj))
         (subdir "")
         )
    (when (string-match root-dir current-dir)
      (setf subdir (substring current-dir (match-end 0))))
    (concat "cd " root-dir "Debug/" "; make -j3")))

;;; Projects

;; cpp-tests project definition
(when (file-exists-p "~/engine/Makefile")
  (setq cpp-loom-engine-project
	(ede-cpp-root-project "cpp-loom-engine"
			      :file "~/engine/Makefile"
			      :system-include-path '("/opt/oblong/g-speak3.10/include")
			      ;;:compile-command "make"
			      )))
            ;;    "/opt/oblong/greenhouse/include"
             ;;   "/opt/oblong/deps/include"
              ;;  "/opt/oblong/assimp/include"

(when (file-exists-p "~/projects/squid-gsb/README")
  (setq squid-gsb-project
	(ede-cpp-root-project "squid-gsb"
			      :file "~/projects/squid-gsb/README"
			      :system-include-path '("/home/ott/exp/include"
						     boost-base-directory)
			      :compile-command "cd Debug && make -j2"
			      )))

;; Setup JAVA....
;;(require 'semantic/db-javap)

;; example of java-root project

;; (ede-ant-project "Lucene"
;; 		       :file "~/work/lucene-solr/lucene-4.0.0/build.xml"
;; 		       :srcroot '("core/src")
;; 		       :classpath (cedet-files-list-recursively "~/work/lucene-solr/lucene-4.0.0/" ".*\.jar$")
;; 		       )
